# bruteForce

This solution include 2 library : pupperteer and Fs


## Getting started

First you need node install on your machine

just type ` apt get install node`

## Build the solution

To build just type
`npm i`

## Brute force from dictonary

This brut force work with a dictonary download on the web, it contain multiple of password found on hacked database
to run it 

`node bruteForceDico.js`

If you want to change the target you could found in the code 
```
 await page.goto("http://localhost/wordpress/wp-admin");
```

just change the local host adress


## Brute force from scratch

This bruteforce will try all posibilty on the ascii table ,
to launch it just type 
`node bruteForce.js`


#Behavior

The brute force will launch a chrome tab and navigate throught the target.

It will test all possibilities and stop when you are connected.


