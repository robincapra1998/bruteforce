var arr = [];
function recursive(istr,curstr,count) {
    count--;
    for(var i=0; i<istr.length; i++) {
      var str = curstr + istr.charAt(i);
      if(count>0) {
        recursive(istr,str,count);
      }
      else {
          console.log(str);
          arr[i++] = str;  // or they are in the array here
      }
    }
  }
  
  let utfString = "";
  for(let i=33; i <= 126; i++){
      utfString += String.fromCharCode(i);
  }
  
  function enumerate(str, n) {
    for(var i=0;i<n;i++) {
      recursive(str,"",i+1);
    }
  } 
  
  enumerate(utfString,4);